var currentTime;
var userTime;
var compareTimeInterval;
var userVideo;

window.onload = function(){
    console.log("ready");

    //write time periodically
    var writeTimeInterval = setInterval(writeTime, 1000);
    compareTimeInterval = setInterval(compareTime, 5000);

    function writeTime(){
        //get current time
        var date = new Date();
        var timeString = date.toTimeString();
        var timeSpace = timeString.indexOf(" ")
        currentTime = timeString.substring(0, timeSpace);
        var displayTime = date.toLocaleTimeString();

        //write current time
        var timeDiv = document.getElementById("time-now");
        timeDiv.innerHTML = displayTime;
    }

};


//get time and video typed by user
function getUserTime() {

    //get user video
    getUserVideo();

    //get user time
    var timeControl = document.querySelector('input[type="time"]');
    userTime = timeControl.value;
    console.log(userTime);

    return false;
}

//get video typed by user
function getUserVideo() {
    //get user time
    var videoControl = document.querySelector('input[type="text"]');
    rawVideo = videoControl.value;
    userVideo = rawVideo.replace("watch?v=", "embed/");
    console.log(userVideo);

    return false;
}

//check if current time equals user time
function compareTime(){

    var currentHoursMinutes = currentTime.substring(0, 5);

    //if times are equal, add the video
    if(currentHoursMinutes == userTime){
        console.log("It's time");
        addVideo();
    }
    else{
        console.log("not yet");
    }

}

function addVideo(){

    var videoIframe = '<iframe class="text-center" width="1020" height="630" src="' + userVideo + '?autoplay=1" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>';

    $('#box').append(videoIframe);

    var onlyOneVideo = clearInterval(compareTimeInterval);
}

